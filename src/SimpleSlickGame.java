import org.newdawn.slick.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public class SimpleSlickGame extends BasicGame {
  ShipGameObject ship;
	BulletGameObject bullet_;
	public SimpleSlickGame(String gamename) {
		super(gamename);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
    GraphicsManager.LoadImages();
    ship = new ShipGameObject(128, 200);
	}

	@Override
	public void update(GameContainer gc, int i) throws SlickException {
    ship.update(gc);
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
    ship.render(gc);
	}

	public static void main(String[] args) {
		try {
			AppGameContainer appgc;
			appgc = new AppGameContainer(new SimpleSlickGame("Simple Slick Game"));
			appgc.setDisplayMode(256, 256, false);
			appgc.setTargetFrameRate(60);
			appgc.start();
		} catch (SlickException ex) {
			Logger.getLogger(SimpleSlickGame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}