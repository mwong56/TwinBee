import org.newdawn.slick.*;

/**
 * Created by miwong on 6/27/14.
 */
public class BulletGameObject extends AbstractGameObject {

	public BulletGameObject(int x, int y) {
		this.x = x;
		this.y = y;
		GraphicsManager.LoadImage("bulletImage", "content/images/bullet.png");
	}

	public void update(GameContainer gc) {
		y -= 10;
	}

	public void render(GameContainer gc) {
		GraphicsManager.DrawImage("bulletImage", getX(), getY());
	}

}
