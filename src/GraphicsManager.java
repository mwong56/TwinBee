/**
 * Created by chackman on 6/27/14.
 */

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.util.*;

import org.newdawn.slick.*;

public class GraphicsManager {
	private static HashMap<String, Image> images = new HashMap<String, Image>();

	public static void LoadImages() {
    LoadImage("ship", "content/images/ship.png");
	}

	public static void LoadImage(String Key, String Path) {
		Image img = null;

		try {
			img = new Image(Path);
		} catch (Exception e) {
			System.out.println("Error loading image");
		}

		images.put(Key, img);
	}

	public static void DrawImage(String Key, int X, int Y) {
		Image img = images.get(Key);
		if (img != null) {
			img.draw((float) X, (float) Y);
		}

	}
}
