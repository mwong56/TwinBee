import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by clee on 6/27/14.
 */
public class ShipGameObject extends AbstractGameObject {

	private int movementUnit = 3;
	private List<BulletGameObject> bulletList_;

	public ShipGameObject(int X, int Y) {
		x = X;
		y = Y;
		bulletList_ = new ArrayList<BulletGameObject>();
	}

	@Override
	public void render(GameContainer gc) {
		GraphicsManager.DrawImage("ship", x, y);
		for (BulletGameObject obj : bulletList_) {
			obj.render(gc);
		}
	}

	@Override
	public void update(GameContainer gc) {
		Input input = gc.getInput();

		boolean w = input.isKeyDown(Input.KEY_W);
		boolean a = input.isKeyDown(Input.KEY_A);
		boolean s = input.isKeyDown(Input.KEY_S);
		boolean d = input.isKeyDown(Input.KEY_D);

		boolean enter = input.isKeyPressed(Input.KEY_ENTER);
		if (enter) {
			bulletList_.add(new BulletGameObject(x, y));
		}

		if (w) this.y -= movementUnit;
		if (s) this.y += movementUnit;
		if (a) this.x -= movementUnit;
		if (d) this.x += movementUnit;

		for (int i = 0; i < bulletList_.size(); ) {
			bulletList_.get(i).update(gc);
			if (bulletList_.get(i).y < 0) {
				bulletList_.remove(i);
			} else {
				i++;
			}
		}

	}
}
