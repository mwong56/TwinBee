/**
 * Created by chackman on 6/27/14.
 */

import org.newdawn.slick.*;

public abstract class AbstractGameObject {
	protected int x, y;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public abstract void render(GameContainer gc);

	public abstract void update(GameContainer gc);
}
